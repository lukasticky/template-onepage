# OnePage Template

This is a one page template for static websites with GitLab Pages already set up.

```txt
.
├── README.md
└── public                      Website root
    ├── assets                  Website assets
    ├── index.html
    └── style                   
        ├── _presets.scss       Variables for colors, fonts, animation, etc.
        ├── _base.scss          Basic styling of HTML & custom elements
        ├── _details.scss       Modifications of HTML & custom elements
        ├── _tablet.scss        Modifications for larger screens (tablets)
        ├── _desktop.scss       Modifications for larger screens (desktops)
        ├── style.scss          
        └── style.css           Generated CSS
```